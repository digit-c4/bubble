# Bubble Documentation

This repository contains the documentation related to the Bubble architecture.

This repository is following the [guidelines](https://code.europa.eu/digit-c4/compliancy/guidelines).

For more information:
  - the [CONTRIBUTING](./CONTRIBUTING.md) document describes how to contribute to the repository
  - consult the [documentation](https://digit-c4.pages.code.europa.eu/bubble)
