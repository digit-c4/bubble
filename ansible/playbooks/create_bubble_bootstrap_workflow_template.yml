# This will only accept 3 parameters as input
# bubble_name (mind the length)
# bubble_subnet (CIDR)
# bubble_tier (bronzer/silver/gold/plat)


# This will create the Workflow Template for a bubble
# It will have to be started manually, no data entry should be needed
- name: Create Workflow Template to boostrap a bubble
  hosts: localhost
  connection: local
  gather_facts: false
  tasks:

    # Preemptively create the AWX Inventory
    # It will need to be referenced even better it's actually populated..
    - name: Create Inventory (even if empty)
      awx.awx.inventory:
        name: "bootstrap-{{ bubble_name }}-inventory"
        organization: Default
        controller_host: "{{ lookup('env', 'AWX_API') | default('') }}"
        controller_password: "{{ lookup('env', 'AWX_PASSWORD') | default('') }}"
        controller_username: "{{ lookup('env', 'AWX_USERNAME') | default('') }}"

    - name: Create VM (not services) Inventory (even if empty)
      awx.awx.inventory:
        name: "bootstrap-{{ bubble_name }}-vm-inventory"
        organization: Default
        controller_host: "{{ lookup('env', 'AWX_API') | default('') }}"
        controller_password: "{{ lookup('env', 'AWX_PASSWORD') | default('') }}"
        controller_username: "{{ lookup('env', 'AWX_USERNAME') | default('') }}"

    - name: Create Target Bubble AWX Credentials (even if empty)
      awx.awx.credential:
        description: "AWX Credential for {{ bubble_name }}"
        credential_type: "AWX API"
        name: "AWX_API_{{ bubble_name }}"
        organization: Default
        controller_host: "{{ lookup('env', 'AWX_API') | default('') }}"
        controller_password: "{{ lookup('env', 'AWX_PASSWORD') | default('') }}"
        controller_username: "{{ lookup('env', 'AWX_USERNAME') | default('') }}"

    - name: Create Target Bubble AWX Credentials (even if empty)
      awx.awx.credential:
        description: "Netbox Credential for {{ bubble_name }}"
        credential_type: "Netbox API"
        name: "Netbox_API_{{ bubble_name }}"
        organization: Default
        controller_host: "{{ lookup('env', 'AWX_API') | default('') }}"
        controller_password: "{{ lookup('env', 'AWX_PASSWORD') | default('') }}"
        controller_username: "{{ lookup('env', 'AWX_USERNAME') | default('') }}"

    - name: Create Job template to do AWS Bubble bootstrap + create temporary Inventory + resize Syslog and RPS VMs volumes
      awx.awx.job_template:
        name: "Bubble_bootstrap_aws_{{ bubble_name }}_part0"
        inventory: "Localhost"
        project: "Bubble_francois_v2"
        organization: "Default"
        job_type: "run"
        playbook: "ansible/playbooks/bootstrap_bubble.yml"
        execution_environment: "ec_awx_ee"
        extra_vars:
          bubble_name: "{{ bubble_name }}"
          bubble_subnet: "{{ bubble_subnet }}"
          bubble_tier: "{{ bubble_tier }}"
        # Should be standardized, kinda hard coded for now...
        credentials:
          - "SYS_AWS_BOOTSTRAP"
          - "AWX_API"
          - "Bubble_bootstrap_sys_misc_services"
        controller_host: "{{ lookup('env', 'AWX_API') | default('') }}"
        controller_password: "{{ lookup('env', 'AWX_PASSWORD') | default('') }}"
        controller_username: "{{ lookup('env', 'AWX_USERNAME') | default('') }}"

    # - name: Create Job template to run distupgrade on VMs of the bubble
    #   awx.awx.job_template:
    #     name: "Bubble_bootstrap_aws_{{ bubble_name }}_part111"
    #     inventory: "bootstrap-{{ bubble_name }}-vm-inventory"
    #     project: "Bubble_francois_v2"
    #     organization: "Default"
    #     job_type: "run"
    #     playbook: "ansible/playbooks/vm_run_distupgrade_reboot.yml"
    #     execution_environment: "ec_awx_ee"
    #     # forks to be tester later for faster performance
    #     #forks: 10
    #     credentials:
    #       - "aws-snet"
    #     controller_host: "{{ lookup('env', 'AWX_API') | default('') }}"
    #     controller_password: "{{ lookup('env', 'AWX_PASSWORD') | default('') }}"
    #     controller_username: "{{ lookup('env', 'AWX_USERNAME') | default('') }}"

    - name: Create Job template to run operations on Proxy VM
      awx.awx.job_template:
        name: "Bubble_bootstrap_aws_{{ bubble_name }}_part1"
        inventory: "bootstrap-{{ bubble_name }}-inventory"
        project: "Bubble_francois_v2"
        organization: "Default"
        job_type: "run"
        playbook: "ansible/playbooks/delete_proxy_restart_vm.yml"
        execution_environment: "ec_awx_ee"
        limit: "proxy.ec.local"
        credentials:
          - "aws-snet"
          - "SYS_AWS_BOOTSTRAP"
          # TODO: add part about key copy
        controller_host: "{{ lookup('env', 'AWX_API') | default('') }}"
        controller_password: "{{ lookup('env', 'AWX_PASSWORD') | default('') }}"
        controller_username: "{{ lookup('env', 'AWX_USERNAME') | default('') }}"

    - name: Create Job template to update docker daemon.json on all VMs
      awx.awx.job_template:
        name: "Bubble_bootstrap_aws_{{ bubble_name }}_part1b"
        inventory: "bootstrap-{{ bubble_name }}-vm-inventory"
        project: "Bubble_francois_v2"
        organization: "Default"
        job_type: "run"
        playbook: "ansible/playbooks/update_docker_daemon_network.yml"
        execution_environment: "ec_awx_ee"
        credentials:
          - "aws-snet"
          # TODO: add part about key copy
        controller_host: "{{ lookup('env', 'AWX_API') | default('') }}"
        controller_password: "{{ lookup('env', 'AWX_PASSWORD') | default('') }}"
        controller_username: "{{ lookup('env', 'AWX_USERNAME') | default('') }}"

    # Create DNS Project for Squid
    - name: Create AWX Project for Squid Proxy
      awx.awx.project:
        name: "Bubble_bs_squid_project"
        description: "Project for Squid Proxy"
        organization: "Default"
        scm_type: "git"
        scm_url: "https://code.europa.eu/digit-c4/sec/dns-instance.git"
        scm_branch: "PAW_adjustment_Bubble_Bootstrap"
        state: present
        credential: "GITLAB_DNS_INSTANCES"
        controller_host: "{{ lookup('env', 'AWX_API') | default('') }}"
        controller_password: "{{ lookup('env', 'AWX_PASSWORD') | default('') }}"
        controller_username: "{{ lookup('env', 'AWX_USERNAME') | default('') }}"
        validate_certs: false


    # Install Netbox
    - name: Create Job template to Install Netbox
      awx.awx.job_template:
        name: "Bubble_bootstrap_aws_{{ bubble_name }}_part2"
        inventory: "bootstrap-{{ bubble_name }}-inventory"
        project: "sys-service-catalogue_fmi"
        organization: "Default"
        job_type: "run"
        playbook: "ansible/playbooks/application_netbox.yml"
        execution_environment: "ec_awx_ee"
        limit: "netbox.ec.local"
        extra_vars:
          traefik: false
          # move to credentials????
          https_proxy: "{{ lookup('env', 'NETBOX_BS_HTTP_PROXY') | default('') }}"
          http_proxy: "{{ lookup('env', 'NETBOX_BS_HTTP_PROXY') | default('') }}"

          # TODO: move to credentials
          POSTGRES_DB: "{{ lookup('env', 'NETBOX_BS_POSTGRES_DB') | default('') }}"
          POSTGRES_PASSWORD: "{{ lookup('env', 'NETBOX_BS_POSTGRES_PASSWORD') | default('') }}"
          POSTGRES_USER: "{{ lookup('env', 'NETBOX_BS_POSTGRES_USER') | default('') }}"
          SUPERUSER_NAME: "{{ lookup('env', 'NETBOX_BS_SUPERUSER_NAME') | default('') }}"
          SUPERUSER_PASSWORD: "{{ lookup('env', 'NETBOX_BS_SUPERUSER_PASSWORD') | default('') }}"

          # TODO: move to credentials
          gitlab_repo: "{{ lookup('env', 'NETBOX_BS_GL_REPO') | default('') }}"
          gitlab_username: "{{ lookup('env', 'NETBOX_BS_GL_USERNAME') | default('') }}"
          gitlab_password: "{{ lookup('env', 'NETBOX_BS_GL_PASSWORD') | default('') }}"
          # TODO: move to credentials
          docker_io_username: "{{ lookup('env', 'NETBOX_BS_DOCKER_USERNAME') | default('') }}"
          docker_io_password: "{{ lookup('env', 'NETBOX_BS_DOCKER_PASSWORD') | default('') }}"
        credentials:
          - "aws-snet"
        controller_host: "{{ lookup('env', 'AWX_API') | default('') }}"
        controller_password: "{{ lookup('env', 'AWX_PASSWORD') | default('') }}"
        controller_username: "{{ lookup('env', 'AWX_USERNAME') | default('') }}"
    # Take care of Netbox token - todo later
    # Assume Credential Type Netbox exists
    - name: Create Job template to create Netbox Token, and populate AWX + Netbox Credentials
      awx.awx.job_template:
        name: "Bubble_bootstrap_aws_{{ bubble_name }}_part3"
        inventory: "bootstrap-{{ bubble_name }}-inventory"
        project: "Bubble_francois_v2"
        organization: "Default"
        job_type: "run"
        playbook: "ansible/playbooks/generate_netbox_bootstrap_token.yml"
        execution_environment: "ec_awx_ee"
        limit: "netbox.ec.local"
        extra_vars:
          bubble_name: "{{ bubble_name }}"
          # do that so the proxy won't block connection to bootstrapping AWX
          no_proxy: "{{ lookup('env', 'AWX_API') | default('') }}"
        credentials:
          - "aws-snet"
          - "Bubble_bootstrap_netbox_installation"
          - "AWX_API"
          # TODO: add part about key copy
        controller_host: "{{ lookup('env', 'AWX_API') | default('') }}"
        controller_password: "{{ lookup('env', 'AWX_PASSWORD') | default('') }}"
        controller_username: "{{ lookup('env', 'AWX_USERNAME') | default('') }}"
    # Populate Netbox data
    - name: Create Job template to fill Netbox data
      awx.awx.job_template:
        name: "Bubble_bootstrap_aws_{{ bubble_name }}_part4"
        inventory: "bootstrap-{{ bubble_name }}-inventory"
        project: "sys-service-catalogue_fmi"
        organization: "Default"
        job_type: "run"
        playbook: "ansible/playbooks/netbox_data.yml"
        execution_environment: "ec_awx_ee"
        limit: "netbox.ec.local"
        extra_vars:
          bubble_name: "{{ bubble_name }}"
          aws: true
          # required for authentication and updates
          http_proxy: "{{ lookup('env', 'NETBOX_BS_HTTP_PROXY') | default('') }}"
          https_proxy: "{{ lookup('env', 'NETBOX_BS_HTTP_PROXY') | default('') }}"
          subnet_cidr: "{{ bubble_subnet }}"

          # TODO: simplify it
          # tasks/create_config_contexts.yml
          whitelist:
            whitelist_urls:
              - .free.fr
              - .service-now.com
              - defense-eu.conferdeploy.net
              - .docker.com
              - .docker.io
              - .docker.sock
              - .europa.eu
              - .ansible.com
              - .amazonaws.com
              - .google.be
              - .debian.org
              - pypi.org
              - .pythonhosted.org
              - .quay.io
              - .ec.local
          custom_config:
            additional_config:
              - "acl SSL_ports port 4567"

          # netbox data:
          # to be simplified
          device_dict:
            - sys:
              name: SYS
              color: '#2196f3'
            - rps:
              name: RPS
              color: '#00ff00'
            - dns:
              name: DNS
              color: '#00ffff'
            - proxy:
              name: PROXY
              color: '#ff0000'
            - vcenter:
              name: vcenter
          cluster_list:
            - "{{ bubble_name | upper }}"
          # redundant as aws boolean is already set to true
          cluster_type: AWS

          platform_dict:
            - name: debian12
              description: 'Debian Bookworm Operating System. EOL: June 2028'
        credentials:
          - "aws-snet"
        controller_host: "{{ lookup('env', 'AWX_API') | default('') }}"
        controller_password: "{{ lookup('env', 'AWX_PASSWORD') | default('') }}"
        controller_username: "{{ lookup('env', 'AWX_USERNAME') | default('') }}"

    # Install Squid - refer to the SQUID Credential types to get all data
    - name: Create Job template to Install Squid Proxy
      awx.awx.job_template:
        name: "Bubble_bootstrap_aws_{{ bubble_name }}_part5"
        inventory: "bootstrap-{{ bubble_name }}-inventory"
        project: "Bubble_bs_squid_project"
        organization: "Default"
        job_type: "run"
        playbook: "ansible/playbooks/PS_Deploy_SQUID_PAWS.yml"
        execution_environment: "ec_awx_ee"
        limit: "proxy.ec.local"
        extra_vars:
          bootstrap: true
          bubble_subnet: "{{ bubble_subnet }}"
          # undefine it so it can be redefined 'properly' in the playbook
          host: proxy.ec.local
          whitelists:
            - netbox_proxy
          god_user: "{{ lookup('env', 'SQUID_BS_GOD_USER') | default('') }}"
          ssh_user: "{{ lookup('env', 'SQUID_BS_SSH_USER') | default('') }}"
          ssh_pass: "{{ lookup('env', 'SQUID_BS_SSH_PASS') | default('') }}"
          ansible_become_pass: "{{ lookup('env', 'SQUID_BS_ANSIBLE_BECOME_PASS') | default('') }}"
          squid_docker_image: ubuntu/squid:5.2-22.04_beta
          squid_listening_port: "{{ lookup('env', 'SQUID_BS_SQUID_LISTENING_PORT') | default('') }}"
          proxy_user: "{{ lookup('env', 'SQUID_BS_PROXY_USER') | default('') }}"
          proxy_pass: "{{ lookup('env', 'SQUID_BS_PROXY_PASS') | default('') }}"
          proxy_ip: "{{ lookup('env', 'SQUID_BS_PROXY_IP') | default('') }}"
          proxy_port: "{{ lookup('env', 'SQUID_BS_PROXY_PORT') | default('') }}"
          worker_ip: "{{ lookup('env', 'SQUID_BS_WORKER_IP') | default('') }}"
          PS_VIP_BRU: "{{ lookup('env', 'SQUID_BS_PS_VIP_BRU') | default('') }}"
          PS_VIP_LUX: "{{ lookup('env', 'SQUID_BS_PS_VIP_LUX') | default('') }}"
          PS_VIP_PORT: "{{ lookup('env', 'SQUID_BS_PS_VIP_PORT') | default('') }}"
          ps_generic_user: "{{ lookup('env', 'SQUID_BS_PS_GENERIC_USER') | default('') }}"
          ps_generic_password: "{{ lookup('env', 'SQUID_BS_PS_GENERIC_PASSWORD') | default('') }}"
          # fill that variable in project
          # syslog_vip: ""
          syslog_protocol: udp
          syslog_port: '514'
          log_format_tid: "aws-{{ bubble_name }}"
        credentials:
          - "aws-snet"
        controller_host: "{{ lookup('env', 'AWX_API') | default('') }}"
        controller_password: "{{ lookup('env', 'AWX_PASSWORD') | default('') }}"
        controller_username: "{{ lookup('env', 'AWX_USERNAME') | default('') }}"


    - name: Create Job template to update proxy conf and apt.conf on all VMs except Proxy
      awx.awx.job_template:
        name: "Bubble_bootstrap_aws_{{ bubble_name }}_part6"
        inventory: "bootstrap-{{ bubble_name }}-vm-inventory"
        project: "Bubble_francois_v2"
        organization: "Default"
        job_type: "run"
        playbook: "ansible/playbooks/update_proxy_apt.yml"
        execution_environment: "ec_awx_ee"
        credentials:
          - "aws-snet"
        extra_vars:
          bubble_name: "{{ bubble_name }}"
          # TODO: add part about key copy
        controller_host: "{{ lookup('env', 'AWX_API') | default('') }}"
        controller_password: "{{ lookup('env', 'AWX_PASSWORD') | default('') }}"
        controller_username: "{{ lookup('env', 'AWX_USERNAME') | default('') }}"

    - name: Create Job template to run distupgrade / reboot VMs of the bubble
      awx.awx.job_template:
        name: "Bubble_bootstrap_aws_{{ bubble_name }}_part7"
        inventory: "bootstrap-{{ bubble_name }}-vm-inventory"
        project: "Bubble_francois_v2"
        organization: "Default"
        job_type: "run"
        playbook: "ansible/playbooks/vm_run_distupgrade_reboot.yml"
        execution_environment: "ec_awx_ee"
        # forks to be tester later for better performance
        # forks: 10
        credentials:
          - "aws-snet"
        extra_vars:
          bubble_name: "{{ bubble_name }}"
        controller_host: "{{ lookup('env', 'AWX_API') | default('') }}"
        controller_password: "{{ lookup('env', 'AWX_PASSWORD') | default('') }}"
        controller_username: "{{ lookup('env', 'AWX_USERNAME') | default('') }}"

    # Install AWX - refer to the AWX Credential types to get all data
    - name: Create Job template to Install AWX on the bubble
      awx.awx.job_template:
        name: "Bubble_bootstrap_aws_{{ bubble_name }}_part8"
        inventory: "bootstrap-{{ bubble_name }}-inventory"
        project: "sys-service-catalogue_fmi"
        organization: "Default"
        job_type: "run"
        playbook: "ansible/playbooks/application_awx.yml"
        execution_environment: "ec_awx_ee"
        limit: "awx.ec.local"
        extra_vars:
          traefik: false
          # in case of bootstrapping, it needs to be fetched within the installation playbook, cannot be infered for now
          DJANGO_SUPERUSER_PASSWORD: "{{ lookup('env', 'AWX_BS_DJANGO_SUPERUSER_PASSWORD') | default('') }}"
          POSTGRES_PASSWORD: "{{ lookup('env', 'AWX_BS_POSTGRES_PASSWORD') | default('') }}"
          POSTGRES_USER: "{{ lookup('env', 'AWX_BS_POSTGRES_USER') | default('') }}"
          NO_PROXY: "127.0.0.1,localhost,.ec.local"
          snet_awx_password: "{{ lookup('env', 'AWX_BS_SNET_AWX_PASSWORD') | default('') }}"
          AWX_SECRET_KEY: "{{ lookup('env', 'AWX_BS_AWX_SECRET_KEY') | default('') }}"
        credentials:
          - "aws-snet"
        controller_host: "{{ lookup('env', 'AWX_API') | default('') }}"
        controller_password: "{{ lookup('env', 'AWX_PASSWORD') | default('') }}"
        controller_username: "{{ lookup('env', 'AWX_USERNAME') | default('') }}"

    - name: Create Job template to bootstrap things on Bubble AWX
      awx.awx.job_template:
        name: "Bubble_bootstrap_aws_{{ bubble_name }}_part9b"
        inventory: "bootstrap-{{ bubble_name }}-vm-inventory"
        project: "Bubble_francois_v2"
        organization: "Default"
        job_type: "run"
        playbook: "ansible/playbooks/create_bubble_awx_items.yml"
        execution_environment: "ec_awx_ee"
        # This is quite ugly but this is the only machine we are sure of the inventory name
        # In case of bronze tier for instance, awx will be part of management vm, there is no awx VM
        limit: "proxy.ec.local"
        extra_vars:
          bubble_name: "{{ bubble_name }}"
        credentials:
          - "aws-snet"
        controller_host: "{{ lookup('env', 'AWX_API') | default('') }}"
        controller_password: "{{ lookup('env', 'AWX_PASSWORD') | default('') }}"
        controller_username: "{{ lookup('env', 'AWX_USERNAME') | default('') }}"

    # This is placeholder, should be more or less empty, will be update in part10
    # ugly, maybe there is a better way to do that
    - name: Create Placeholder Job template - will be updater later on
      awx.awx.job_template:
        name: "Bubble_bootstrap_aws_{{ bubble_name }}_part10"
        inventory: "bootstrap-{{ bubble_name }}-inventory"
        project: "Bubble_francois_v2"
        organization: "Default"
        job_type: "run"
        playbook: "ansible/playbooks/create_bubble_awx_items_part2.yml"
        execution_environment: "ec_awx_ee"
        limit: "awx.ec.local"
        extra_vars:
          bubble_name: "{{ bubble_name }}"
        credentials:
          - "aws-snet"
        controller_host: "{{ lookup('env', 'AWX_API') | default('') }}"
        controller_password: "{{ lookup('env', 'AWX_PASSWORD') | default('') }}"
        controller_username: "{{ lookup('env', 'AWX_USERNAME') | default('') }}"

    # Inception style, create Job Template within the job template to be able to pass credentials about the Bubble

    - name: Create Job template to bootstrap things on Bubble AWX
      awx.awx.job_template:
        name: "Bubble_bootstrap_aws_{{ bubble_name }}_part9"
        inventory: "bootstrap-{{ bubble_name }}-inventory"
        project: "Bubble_francois_v2"
        organization: "Default"
        job_type: "run"
        playbook: "ansible/playbooks/create_bubble_awx_items.yml"
        execution_environment: "ec_awx_ee"
        limit: "awx.ec.local"
        extra_vars:
          bubble_name: "{{ bubble_name }}"
        credentials:
          - "aws-snet"
          - "AWX_API"
        controller_host: "{{ lookup('env', 'AWX_API') | default('') }}"
        controller_password: "{{ lookup('env', 'AWX_PASSWORD') | default('') }}"
        controller_username: "{{ lookup('env', 'AWX_USERNAME') | default('') }}"

    - name: Create Job template to bootstrap things on Bubble AWX
      awx.awx.job_template:
        name: "Bubble_bootstrap_aws_{{ bubble_name }}_part11"
        inventory: "Localhost"
        project: "sys-service-catalogue_fmi"
        organization: "Default"
        job_type: "run"
        playbook: "ansible/playbooks/aws_reboot_bubble_vms.yml"
        execution_environment: "ec_awx_ee"
        extra_vars:
          bubble_name: "{{ bubble_name }}"
          aws_region: "eu-west-1"
        credentials:
          - "aws-snet"
          - "SYS_AWS_BOOTSTRAP"
        controller_host: "{{ lookup('env', 'AWX_API') | default('') }}"
        controller_password: "{{ lookup('env', 'AWX_PASSWORD') | default('') }}"
        controller_username: "{{ lookup('env', 'AWX_USERNAME') | default('') }}"

    - name: Create Placeholder Job template - will be updater later on
      awx.awx.job_template:
        name: "Bubble_bootstrap_aws_{{ bubble_name }}_part12"
        inventory: "bootstrap-{{ bubble_name }}-inventory"
        project: "Bubble_francois_v2"
        organization: "Default"
        job_type: "run"
        playbook: "ansible/playbooks/create_bubble_awx_items_part3.yml"
        execution_environment: "ec_awx_ee"
        limit: "awx.ec.local"
        extra_vars:
          bubble_name: "{{ bubble_name }}"
        credentials:
          - "aws-snet"
        controller_host: "{{ lookup('env', 'AWX_API') | default('') }}"
        controller_password: "{{ lookup('env', 'AWX_PASSWORD') | default('') }}"
        controller_username: "{{ lookup('env', 'AWX_USERNAME') | default('') }}"

    - name: Create Placeholder Job template - will be updater later on
      awx.awx.job_template:
        name: "Bubble_bootstrap_aws_{{ bubble_name }}_part13"
        inventory: "bootstrap-{{ bubble_name }}-inventory"
        project: "Bubble_francois_v2"
        organization: "Default"
        job_type: "run"
        playbook: "ansible/playbooks/create_bubble_awx_items_part4_rps.yml"
        execution_environment: "ec_awx_ee"
        limit: "awx.ec.local"
        extra_vars:
          bubble_name: "{{ bubble_name }}"
        credentials:
          - "aws-snet"
        controller_host: "{{ lookup('env', 'AWX_API') | default('') }}"
        controller_password: "{{ lookup('env', 'AWX_PASSWORD') | default('') }}"
        controller_username: "{{ lookup('env', 'AWX_USERNAME') | default('') }}"


    - name: Bubble bootstrap template workflow 1/2
      # https://docs.ansible.com/ansible/latest/collections/awx/awx/workflow_job_template_module.html
      awx.awx.workflow_job_template:
        name: "Bubble_{{ bubble_name }}_bootstrap_workflow"
        destroy_current_nodes: true
        workflow_nodes:

          - identifier: "Bubble_bootstrap_workflow_part0"
            unified_job_template:
              name: "Bubble_bootstrap_aws_{{ bubble_name }}_part0"
              type: job_template
            related:
              success_nodes:
                - identifier: "Bubble_bootstrap_workflow_part1"

          - identifier: "Bubble_bootstrap_workflow_part1"
            unified_job_template:
              name: "Bubble_bootstrap_aws_{{ bubble_name }}_part1"
              type: job_template
            related:
              success_nodes:
                - identifier: "Bubble_bootstrap_workflow_part1b"

          - identifier: "Bubble_bootstrap_workflow_part1b"
            unified_job_template:
              name: "Bubble_bootstrap_aws_{{ bubble_name }}_part1b"
              type: job_template
            related:
              success_nodes:
                - identifier: "Bubble_bootstrap_workflow_part2"

          - identifier: "Bubble_bootstrap_workflow_part2"
            unified_job_template:
              name: "Bubble_bootstrap_aws_{{ bubble_name }}_part2"
              type: job_template
            related:
              success_nodes:
                - identifier: "Bubble_bootstrap_workflow_part3"


          - identifier: "Bubble_bootstrap_workflow_part3"
            unified_job_template:
              name: "Bubble_bootstrap_aws_{{ bubble_name }}_part3"
              type: job_template
            related:
              success_nodes:
                - identifier: "Bubble_bootstrap_workflow_part4"

          - identifier: "Bubble_bootstrap_workflow_part4"
            unified_job_template:
              name: "Bubble_bootstrap_aws_{{ bubble_name }}_part4"
              type: job_template
            related:
              success_nodes:
                - identifier: "Bubble_bootstrap_workflow_part5"

          - identifier: "Bubble_bootstrap_workflow_part5"
            unified_job_template:
              name: "Bubble_bootstrap_aws_{{ bubble_name }}_part5"
              type: job_template
            related:
              success_nodes:
                - identifier: "Bubble_bootstrap_workflow_part6"

          - identifier: "Bubble_bootstrap_workflow_part6"
            unified_job_template:
              name: "Bubble_bootstrap_aws_{{ bubble_name }}_part6"
              type: job_template
            related:
              success_nodes:
                - identifier: "Bubble_bootstrap_workflow_part7"
              # failure_nodes:
              #   - job_template: "Bubble_bootstrap_aws_{{ bubble_name }}_part6"
              #     identifier: "Bubble_bootstrap_workflow_part6_retry"
              #     success_nodes:
              #       - "Bubble_bootstrap_workflow_part7"
              #     failure_nodes:
              #       - job_template: "Bubble_bootstrap_aws_{{ bubble_name }}_part6"
              #         identifier: "Bubble_bootstrap_workflow_part6_retry2"
              #         success_nodes:
              #           - "Bubble_bootstrap_workflow_part7"

          - identifier: "Bubble_bootstrap_workflow_part7"
            unified_job_template:
              name: "Bubble_bootstrap_aws_{{ bubble_name }}_part7"
              type: job_template
            related:
              success_nodes:
                - identifier: "Bubble_bootstrap_workflow_part8"

          - identifier: "Bubble_bootstrap_workflow_part8"
            unified_job_template:
              name: "Bubble_bootstrap_aws_{{ bubble_name }}_part8"
              type: job_template
            related:
              success_nodes:
                - identifier: "Bubble_bootstrap_workflow_part9"

          - identifier: "Bubble_bootstrap_workflow_part9"
            unified_job_template:
              name: "Bubble_bootstrap_aws_{{ bubble_name }}_part9"
              type: job_template
            related:
              success_nodes:
                - identifier: "Bubble_bootstrap_workflow_part9b"

          - identifier: "Bubble_bootstrap_workflow_part9b"
            unified_job_template:
              name: "Bubble_bootstrap_aws_{{ bubble_name }}_part9b"
              type: job_template
            related:
              success_nodes:
                - identifier: "Bubble_bootstrap_workflow_part10"

          - identifier: "Bubble_bootstrap_workflow_part10"
            unified_job_template:
              name: "Bubble_bootstrap_aws_{{ bubble_name }}_part10"
              type: job_template
            related:
              success_nodes:
                - identifier: "Bubble_bootstrap_workflow_part11"

          - identifier: "Bubble_bootstrap_workflow_part11"
            unified_job_template:
              name: "Bubble_bootstrap_aws_{{ bubble_name }}_part11"
              type: job_template
            related:
              success_nodes:
                - identifier: "Bubble_bootstrap_workflow_part12"

          - identifier: "Bubble_bootstrap_workflow_part12"
            unified_job_template:
              name: "Bubble_bootstrap_aws_{{ bubble_name }}_part12"
              type: job_template
            related:
              success_nodes:
                - identifier: "Bubble_bootstrap_workflow_part13"

          - identifier: "Bubble_bootstrap_workflow_part13"
            unified_job_template:
              name: "Bubble_bootstrap_aws_{{ bubble_name }}_part13"
              type: job_template
            # related:
            #   success_nodes:
            #     - identifier: "Bubble_bootstrap_workflow_part12"

          # - identifier: "Bubble_bootstrap_workflow_part11"
          #   unified_job_template:
          #     name: "Bubble_bootstrap_aws_{{ bubble_name }}_part11"
          #     type: job_template
          # - identifier: "Bubble_{{ bubble_name }}bootstrap_workflow_part11"
          #   unified_job_template:
          #     name: SYS_play_add_netbox_docker_hosts
          #     type: job_template
        validate_certs: false
        controller_host: "{{ lookup('env', 'AWX_API') | default('') }}"
        controller_password: "{{ lookup('env', 'AWX_PASSWORD') | default('') }}"
        controller_username: "{{ lookup('env', 'AWX_USERNAME') | default('') }}"
