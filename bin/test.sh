#!/usr/bin/env bash

shopt -s nullglob

_verbose(){
    # This function should be a simple debug function, which will print the line given based on debug level
    # implement in getops the following line:
    # (( DEBUG_LEVEL++))
    local LEVEL=1 c printout
    local funcnamenumber
    (( funcnamenumber=${#FUNCNAME[@]} - 2 ))
    : "${DEBUG_LEVEL:=0}"
    (( DEBUG_LEVEL == 0 )) && return
    # Add level 1 if first char is not set a number
    [[ "$1" =~ ^[0-9]$ ]] && { LEVEL=$1; shift; }

    (( LEVEL <= DEBUG_LEVEL )) && {
        until (( ${#c} == LEVEL )); do c+=":"; done
        if (( funcnamenumber > 0 )); then
            printout+="("
            for ((i=1;i<=funcnamenumber;i++)); do
                printout+="${FUNCNAME[$i]} <- "
            done
            printout="${printout% <- }) - "
        fi
        printf '%-7s %s %s\n' "+ $c" "$printout" "$*" 1>&2
    }
}

info(){ printf '%s\n' "$*"; }
err(){ printf '%s\n' "$*" >&2; }
run(){
    local ret
    info "Test Run: $*"
    "$@"
    ret=$?
    (( ret > 0 )) && {
        err "Test Failed: $*"
        status=1
        return 1
    }
    info "Test Passed: $*"
}

buildAnsible(){
    _verbose 1 "State enter buildAnsible"
    
    for _dir in tests/ansible/*; do
        _verbose 2 "$_dir"
            
        for _tpl in "$_dir"/vars/*.tpl; do
            _verbose 2 "$_tpl"
            envsubst < "$_tpl" > "${_tpl%.tpl}"
        done
        cd "$_dir"
        source "test.sh"
        cd -
        for _tpl in "$_dir"/vars/*.tpl; do
            $_run rm "${_tpl%.tpl}"
        done
    done
}

main(){
    local status=0
    local _run
    buildAnsible

    exit "$status"
}

main
