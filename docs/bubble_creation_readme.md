# Bubble Bootstrap manual

Here is an overview on bootstrapping a bubble.

The Galaxy Bubble in AWS is supposed to be the bubble that is used to bootstrap other bubbles.

However due to various issues that SYS is looking into, another bubble `lb-dns-dev` has been used.

Here is the address of the AWX http://dns-lb-dev-lb-private-8d59a6762e912e6c.elb.eu-west-1.amazonaws.com:8013

A few things are needed to bootstrap a bubble:

## Projects:

* Bootstrap project (this one)

## Credentials:

* BS_DNS_PAT
* BS_FW_PAT
* Bubble_bootstrap_awx_installation
* Bubble_bootstrap_netbox_installation
* Bubble_bootstrap_squid_installation
* Bubble_bootstrap_sys_misc_services
* AWX_API
* AWS_FW
* FM_AWS_Provider

## Templates

Template to create a workflow for a specific bubble:\

Name: Bubble_bootstrap_workflow_creator\
Inventory: Localhost\
Execution Environment: ec_awx_ee\
Playbook: ansible/playbooks/create_bubble_bootstrap_workflow_template.yml\

Credentials:
  * SSH: aws-snet
  * Cloud: AWX_API
  * Cloud: Bubble_bootstrap_awx_installation
  * Cloud: Bubble_bootstrap_netbox_installation
  * Cloud: Bubble_bootstrap_squid_installation

Variables (example):

```yaml
bubble_name: aws-automate
bubble_subnet: 10.67.7.64/27
bubble_tier: platinum
```

## Run the Bubble_bootstrap_workflow_creator template

After the variables are filled and saved, running **Bubble_bootstrap_workflow_creator** is needed.\

This will create a new set of Objects in AWX:

* Inventories for that specific bubble
* New Job workflow to actually create that bubble

Given a **aws-automate** bubble, one Job Workflow **Bubble_aws-automate_bootstrap_workflow** will be created, that will run Job Templates ranging from **Bubble_bootstrap_aws_aws-automate_part0** to **Bubble_bootstrap_aws_aws-automate_part13**

## Start bootstrapping the bubble

After **Bubble_aws-automate_bootstrap_workflow** (replace by the new bubble name) is created, it should be run.\

This process will take between 90 to 120 minutes to complete overall.\

It is not fail proof, meaning that some steps might fail although playbooks have been hardened to implement retries when possible.\

The main recurring issue is connection to the proxy for early staged (before the bubble proxy is setup).\

If a step of the workflow fails, it is not possible to resume the workflow.\

What I recommend is to restart the workflow if the failure is very early in the bootstrap or run remaining steps manually if the bootstrap is almost done.\

This is a work in progress, a lot has to be improved, let us not forget that we went from 4 hours of UI clicking and copy paste or templates to 2 hours of automated workflow.

## Accessing the Bubble

After the bootstrap is done, one can go to `lb-dns-dev` AWX server and check any host of **bootstrap-aws-automate-inventory**.\

AWX and Netbox addresses will be listed so that the user can browse them.

# Next steps

1.
The code works but is not very clear / clean.
It needs refactoring to be able to add more components with ease.

2.
Some custom branches are used which can lead to confusion.
Because custom branches are used, we might not take advantage of new releases.

3.
For now all secrets are in Credentials created by Credential Types.
If values change, they have to be changed manually. The idea would be to store all those in a Bubble Vault and pull them from the AWX.