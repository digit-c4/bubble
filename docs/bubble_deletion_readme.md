# Bubble Deletion manual

Here is an overview on deleting a bubble.

The Galaxy Bubble in AWS is supposed to be the bubble that is used to bootstrap other bubbles.

However due to various issues that SYS is looking into, another bubble `lb-dns-dev` has been used.

Here is the address of the AWX http://dns-lb-dev-lb-private-8d59a6762e912e6c.elb.eu-west-1.amazonaws.com:8013

Please be careful when deleting a bubble, there is no way back.

A few things are needed to delete a bubble:

## Projects:

* Bootstrap project (this one)

## Credentials:

* FM_AWS_Provider (to be updated)

## Templates

Template to delete  a specific bubble:\

Name: Bubble_deletion_template\
Inventory: Localhost\
Execution Environment: ec_awx_ee\
Playbook: ansible/playbooks/delete_bubble_template.yml\

Credentials:
  * SSH: aws-snet
  * Cloud: FM_AWS_Provider
  * Cloud: AWX_API


Variables (example):

```yaml
bubble_name: aws-automate
bubble_region: eu-west-1
```

## Run the Bubble_bootstrap_workflow_creator template

After the variables are filled and saved, running **Bubble_deletion_template** is needed.\

This will create a new set of Objects in AWX:

* New Job workflow to actually delete that bubble

Given a **aws-automate** bubble, one Job Workflow **Bubble_delete_aws-automate_template** will be created

## Start deleting the bubble

After **Bubble_delete_aws-automate_template** (replace by the new bubble name) is created, it should be run.\

This process will take about 5 minutes to complete.\