# Draft 1

```mermaid
graph LR
    NetboxGalaxy[Netbox Galaxy] --> CERTBronze["RPS Bronze"]
    NetboxGalaxy --> RASSilver["VPNAAS Silver"]
    NetboxGalaxy --> RPSGold["CERTAAS Gold"]
    NetboxGalaxy --> Platinum["Platinum"]
 
    BubbleName["BubbleName"] --> NetboxGalaxy
 
    CERTBronze --> BronzeServers["2x2.small (1 priv, 1 pub)"]
    RASSilver --> SilverServers["2x2.medium (1 priv, 1 pub)"]
    RPSGold --> GoldServers["2x2.2xlarge (1 priv, 1 pub) "]
    Platinum --> PlatinumServers["1x2.medium per service"]
```