
# TECHNICAL SEQUENCE

### Complete AWS bubble creation and configuration 
Description:

This is a documentation which explains the different steps to create and configure a Bubble in AWS for the moment.

### Installations using the AWX of the aws-galaxy bubble 
Important:

For the Templates which are going to be run on the aws-galaxy AWX. When the proxy is defined in the vars, use the aws-galaxy proxy. ( except for the AWX installation )


## INSTALLATION VIA AWS-GALAXY


* Netbox: http://netbox-galaxy
* AWX:  http://awx-galaxy

01 - Reserve a /27 subnet in Proteus.

02 - Use the [http://awx-galaxy/#/templates/job_template/58/details bootstrap_bubble] template in the aws-galaxy bubble to create a Bubble in AWS - Be careful TargetGroupName no more than 32 characters so please choose a short bubble name for the variable name.

03 - ADD 50G to the rps VM and the awx VM and 20G to syslog VM and restart them. *retreive the instance id from AWS to use in the playbook [http://awx-galaxy/#/templates/job_template/51/details aws_aws_resize_volume.yml]  

04 - Connect to the newly created Proxy VM and delete the /etc/systemd/system/docker.service.d/http-proxy.conf and restart the VM.

05 - Create an id_rsa file in .ssh/ with the private key from AWS on the proxy VM and do 
```
 chmod 600 .ssh/id_rsa
```

06 - On the AWX of the aws-galaxy bubble, create a temporary Inventory to define hosts: proxy.ec.local, awx.ec.local (group service_awx), netbox.ec.local (group service_netbox)

07 - Install Netbox using the [http://awx-galaxy/#/templates/job_template/69/details "sys_service_catalogue_update_netbox"] template on the AWX from aws-galaxy. ( change the inventory so that it matches the newly created for the new bubble )

08 - When Netbox is completely setup - create an access token.

09 - Push Netbox VM and AWS VIPs (Use the DNS for the ELB with 
```
nslookup
```
to get IP address) data using playbook [http://awx-galaxy/#/templates/job_template/62/details netbox_data.yml] (update extra vars accordingly)

10 - Create vip_external and vip_internal tags and assign it to the IP address dedicated (add VIP role too) in the NETBOX

11 - Install Squid using the [http://awx-galaxy/#/templates/job_template/47/details "Squid_deploy"] template on the aws-galaxy AWX. ( change the inventory so that it matches the newly created for the new bubble and change netbox URL and token in the vars ).

12 - Connect to the newly created AWX VM and edit the /etc/systemd/system/docker.service.d/http-proxy.conf with the proxy address of the newly created proxy VM and restart VM.

13 - Add the proxy also to  the apt file /etc/apt/apt.conf.<syntaxhighlight lang="bash">
Acquire::http::proxy "http://IP:xxxx";
Acquire::https::proxy "http://IP:xxxx";

14 - Install AWX using the [http://awx-galaxy/#/templates/job_template/50/edit "Install awx"]  template on the aws-galaxy AWX. ( In the vars use the address of the newly created proxy VM as proxy )


## INSTALLATION USING THE NEWLY CREATED AWX

I link the templates from aws-galaxy as an example for the different templates.

When http_proxy var is provided use IP address of the proxy VM.

01 - On AWX add a new Project sys-service-catalogue with this git repo https://code.europa.eu/digit-c4/sys-service-catalogue.git

02 - On AWX create a new Inventory [http://awx-galaxy/#/inventories/inventory/2/details "sys_service_catalogue_inventory"] and add the Source of Netbox. ( use netbox_inventory.yml as Inventory file )

03 - Add xxx-xxx credentials with the public key of xxx and user xxx.

04 - Create 2 Templates on AWX "[http://awx-galaxy/#/templates/job_template/12/details Get proxy]" and "[http://awx-galaxy/#/templates/job_template/11/details Change proxy]" and then the Workflow [http://awx-galaxy/#/templates/workflow_job_template/13/details "AWS - change proxy Workflow"]. Launch the Workflow. ( change Proxy for all VM except awx )


05 - Create [http://awx-galaxy/#/templates/job_template/14/details "Install_syslog_server"] Template and launch it.
```
[ For the moment on C4 AWS ]
create template rsyslog template and launch it
create new project sys-catalogue with feature_docker_using_rsyslog_socket branch
create and launch docker template
then launch Install_syslog_server template
```


06 - Create [http://awx-galaxy/#/templates/job_template/15/details "Node Exporter Installation"] Template and launch it.

07 - Create [http://awx-galaxy/#/templates/job_template/16/details "Prometheus Installation"] Template and launch it.

08 - Create [http://awx-galaxy/#/templates/job_template/17/details "Alert Manager Installation"] Template and launch it. ( Prometheus IP for the 2 vars )

09 - Create [http://awx-galaxy/#/templates/job_template/25/details "Webhook_snmp_trap"] Template and launch it. ( IP of SMARTS use same as in aws-galaxy )

10 - Create [http://awx-galaxy/#/templates/job_template/9/details "sys_service_catalogue_update_netbox"] Template.

11 - Create [http://awx-galaxy/#/templates/job_template/19/details "SYS - Install Openobserve®"] and [http://awx-galaxy/#/templates/job_template/44/details "openobserve_configuration"] Templates and launch it. ( first install template )

12 - Create [http://awx-galaxy/#/templates/job_template/52/details "netbox_docker_agent_install"] template and launch it. ( launch 2 times 1 time with host awx disabled )

13 - Create [http://awx-galaxy/#/templates/job_template/18/details "netbox_docker_agent_update"] template.

14 - Create [http://awx-galaxy/#/templates/job_template/26/details "docker_image_prune"] template.

15 - Create GITLAB_DNS_INSTANCES credentials. (info in hashicorp®)

16 - Create PS_Squid Project in AWX. https://code.europa.eu/digit-c4/sec/dns-instance.git . with PAW_adjustment branch

17 - Create [http://awx-galaxy/#/templates/job_template/47/details "Squid_deploy"] template.

18 - Create "awx_backup" template

19 - Add tag Monitoring to VM in netbox and check smarts for errors. ( when no DNS there will be errors in smarts )

20 - Forward Card/Ticket to DNS squad to implement tasks

21 - Push new DNS server on servers via http://awx-galaxy/#/templates/job_template/53/details 

22 - Forward Card/Ticket to RPS squad to implement tasks


## AWX SCHEDULES

Create all Schedules to launch daily.
```
netbox_docker_agent_update: daily at 10PM
update Netbox: daily at 6AM
docker_image_prune: daily at 9:45PM
awx_backup_task: daily at 4:00AM
```

