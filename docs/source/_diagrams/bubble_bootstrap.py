from diagrams import Diagram
from diagrams.programming import flowchart
from diagrams.c4 import Person, Container, Database, System, Relationship, SystemBoundary

graph_attr = {
    "splines": "spline",
}

with Diagram("Bubble Bootstrap diagram", filename="bubble_bootstrap", show=False, graph_attr=graph_attr):
    customer = Person(
        name="SNET User", description="A customer of the C4 bubble primitive ask for a bubble"
    )

    with SystemBoundary("Galaxy"):
        netboxGalaxy = Container(
            name="Netbox Galaxy",
            technology="Netbox on docker",
            description="The digit C4 source of thruth"
        )

        awxGalaxy = Container(
            name="Awx Galaxy",
            technology="Awx on docker",
            description="The ansible runner"
        )

    with SystemBoundary("Bubble"):
        dnsResolver = Container(
            name="DNS Resolver",
            technology="Bind9 on docker",
            description="Dns Resolver"
        )

        rps = Container(
            name="RPS",
            technology="Nginx on docker",
            description="Reverse Proxy"
        )

        proxy = Container(
            name="Proxy",
            technology="Squid on docker",
            description="Proxy"
        )

        netbox = Container(
            name="Netbox",
            technology="Netbox on docker",
            descriptiion="Source of truth"
        )

        awx = Container(
            name="AWX",
            technology="AWX on docker",
            description="The ansible runner"
        )

    virtual_machine = System(
        name="Virtual Machine",
        technology="Debian12 / VApp",
        description="A virtual machine",
        external=True
    )

    loadbalancer = System(
        name="LoadBalancer",
        technology="ELB / F5 / ...",
        description="A load Balancer",
        external=True
    )

    security = System(
        name="Security",
        technology="SecurityGroup / Fortigate / ...",
        description="A security protection",
        external=True
    )

    input = flowchart.InputOutput(
        label="Bubble Name , Prod DNS, Acc DNS"
    )

    output = flowchart.InputOutput(
        label="AWX and Netbox URL"
    )

    input >> netboxGalaxy
    netboxGalaxy >> output
    customer >> Relationship("1. Visits Netbox and adds a new Bubble") >> netboxGalaxy
    netboxGalaxy >> Relationship("2. Starts a webhook call to awx") >> awxGalaxy
    awxGalaxy >> Relationship("4. Create a DNS Resolver") >> dnsResolver
    awxGalaxy >> Relationship("3. Create all virtual Machines") >> virtual_machine
    awxGalaxy >> Relationship("5. Create a proxy machine") >> proxy
    awxGalaxy >> Relationship("6. Create a LoadBalancer") >> loadbalancer
    awxGalaxy >> Relationship("7. Run AWX-data to insert data in awx") >> awx
    awxGalaxy >> Relationship("8. Run Netbox)data to insert data in netbox") >> netbox
    awxGalaxy >> Relationship("9. Run rps playbook to configure rps") >> rps
    awxGalaxy >> Relationship("10. Cleanup DNS resolver or redeploy") >> dnsResolver
    awxGalaxy >> Relationship("11. Configure security rules to allow access to loadbalancer") >> security
