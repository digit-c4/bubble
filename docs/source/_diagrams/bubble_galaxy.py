from diagrams import Diagram
from diagrams.programming import flowchart
from diagrams.c4 import Person, Container, Database, System, Relationship, SystemBoundary

graph_attr = {
    "splines": "spline",
    "rankdir": "TB",
}

with Diagram(
    "Bubble Galaxy diagram",
    filename="bubble_galaxy",
    graph_attr=graph_attr,
    show=False,
):

    with SystemBoundary("Galaxy AWS"):
        awxAwsGalaxy = Container(
            name="Awx Galaxy - AWS",
            technology="Awx on docker",
            description="The ansible runner"
        )

        gitlabrunnerAwsGalaxy = Container(
            name="Gitlab Galaxy - AWS",
            technology="Gitlab runner",
            description="The gitlab runner"
        )

    with SystemBoundary("Galaxy OnPremise"):
        awxOnpremiseGalaxy = Container(
            name="Awx Galaxy - OnPremise",
            technology="Awx on docker",
            description="The ansible runner"
        )
        gitlabrunnerAwsGalaxy = Container(
            name="Gitlab Galaxy - OnPremise",
            technology="Gitlab runner",
            description="The gitlab runner"
        )

    with SystemBoundary("Galaxy Ovh"):
        awxOvhGalaxy = Container(
            name="Awx Galaxy - OVH",
            technology="Awx on docker",
            description="The ansible runner"
        )
        gitlabrunnerAwsGalaxy = Container(
            name="Gitlab Galaxy - OVH",
            technology="Gitlab runner",
            description="The gitlab runner"
        )



