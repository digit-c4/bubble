# Certificate Squad deliverables

The key words “MUST”, “MUST NOT”, “REQUIRED”, “SHALL”, “SHALL NOT”, “SHOULD”,
“SHOULD NOT”, “RECOMMENDED”, “MAY”, and “OPTIONAL” in those documents are to be
interpreted as described in [RFC 2119](https://www.ietf.org/rfc/rfc2119.txt).

## Netbox

The following Certificate Authorities **SHOULD** exist in Netbox:

- commissign®
- globalsign®
- letsencrypt®

For each Certificate delivered, a Netbox Certificate item **MUST** exist.

## AWX

## Job Templates

Job Templates **SHOULD** exist in AWX for each host group (group of RPS and/or WAF hosts).

For example if we have a group named deliv_external, playbooks named:

- CERTIFICATES: ACME on deliv_external
- CERTIFICATES: ACME_schedule on deliv_external
- CERTIFICATES: ACME_schedule_errored on deliv_ternal

**SHOULD** exist.

## Projects

The CERTIFICATES project **MUST** exist and **MUST** be valid.

## Credentials

If the bubble doesn't have a Vault access, acme_privkey credentials **MUST** exist for each ACME CA supported inside of the bubble.

## Schedules

The ACME_schedule Job Templates **MUST** have associated Schedules.


