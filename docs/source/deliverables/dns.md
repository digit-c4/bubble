# DNS Squad deliverables

The key words “MUST”, “MUST NOT”, “REQUIRED”, “SHALL”, “SHALL NOT”, “SHOULD”,
“SHOULD NOT”, “RECOMMENDED”, “MAY”, and “OPTIONAL” in those documents are to be
interpreted as described in [RFC 2119](https://www.ietf.org/rfc/rfc2119.txt).

## SYS

After DNS is configured, AWS Bubble VPC DHCP Options **MUST** be set to the IP of the DNS Private VM.

After DHCP Options are updated, all Virtual Machines of the Bubble **MUST** be rebooted.

## RPS

### Records

The following name servers **MUST** exist:

  - `dnsinternal1.ec.local`
  - `dnsinternal1.{{bubble_name}}.nms.tech.ec.europa.eu`
  - `dnsinternal1.{{bubble_name}}.intracloud.ec.europa.eu`
  - `dnsinternal1.commissign.cec.eu.int`
  - `dnsexternal1.{{bubble_name}}.webcloud.ec.europa.eu`

The following Zones **MUST** exist:

  - `ec.local`
  - `{{ bubble_name }}.nms.tech.ec.europa.eu`
  - `{{ bubble_name }}.intracloud.ec.europa.eu`
  - `{{ bubble_name }}.webcloud.ec.europa.eu`
  - `dnsinternal1.commissign.cec.eu.int`

The following Records **SHOULD** exist:

  - `netbox.ec.local`
  - `awx.ec.local`
  - `openobserve.ec.local`
  - `prometheus.ec.local`
  - `alertmanager.ec.local`
  - `syslog.ec.local`
  - `proxy.ec.local`
  - `rps.ec.local`
  - `rps-2.ec.local`
  - `rps-3.ec.local`
  - `rps-4.ec.local`
  - `dns-public.ec.local`
  - `dns-private.ec.local`
