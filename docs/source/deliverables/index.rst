Deliverables
============

The following documents specify what the squad will deliver as a result of the
implementation of their phase of the bubble bootstrap.

.. toctree::
   :maxdepth: 1

   sys
   rps
   certificates
   dns
