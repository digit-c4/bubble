# RPS Squad deliverables

The key words “MUST”, “MUST NOT”, “REQUIRED”, “SHALL”, “SHALL NOT”, “SHOULD”,
“SHOULD NOT”, “RECOMMENDED”, “MAY”, and “OPTIONAL” in those documents are to be
interpreted as described in [RFC 2119](https://www.ietf.org/rfc/rfc2119.txt).

## DNS

For each environment, the following DNS record **SHOULD** exist:

| Record | Condition |
| --- | --- |
| `waf.<env>.rps.ec.local` | if the bubble is reachable via `external` |
| `proxy.<env>.rps.ec.local` | if the bubble is reachable via `external` |
| `waf.<env>.rps-2.ec.local` | if the bubble is reachable via `external` |
| `proxy.<env>.rps-2.ec.local` | if the bubble is reachable via `external` |
| `waf.<env>.rps-3.ec.local` | if the bubble is reachable via `internal` |
| `proxy.<env>.rps-3.ec.local` | if the bubble is reachable via `internal` |
| `waf.<env>.rps-4.ec.local` | if the bubble is reachable via `internal` |
| `proxy.<env>.rps-4.ec.local` | if the bubble is reachable via `internal` |

For example:

 - `waf.production.rps.ec.local`
 - `proxy.acceptance.rps-4.ec.local`

## Docker

On each RPS Virtual Machine, a *Traefik®* container **MUST** exist with the
following exposed ports:

 - HTTP: 80
 - HTTPS: 443

On each RPS Virtual Machine, a *WAF* container **MUST** exist for each
environment.

On each WAF container in the *NMS* environment, the following labels **MUST** exist:

| Label |Value |
| --- | --- |
| `traefik.tcp.routers.waf_nms_cs.entrypoints` | `https` |
| `traefik.tcp.routers.waf_nms_cs.service` | `waf_nms_cs` |
| `traefik.tcp.routers.waf_nms_cs.rule` | `HostSNI(acme.commissign.cec.eu.int)` |
| `traefik.tcp.routers.waf_nms_cs.tls` | `true` |
| `traefik.tcp.routers.waf_nms_cs.tls.passthrough` | `true` |
| `traefik.tcp.services.waf_nms_cs.loadbalancer.proxyprotocol.version` | `2` |
| `traefik.tcp.routers.waf_nms_cs.entrypoints` | `https` |
| `traefik.tcp.services.waf_nms_cs.loadbalancer.server.port` | `443` |

**NB:** They provide routing for the Commissign® ACME server Reverse Proxy Mapping.

Those containers **MUST** have their host name set to `waf.<env>.<vm fqdn>`, for
example:

 - `waf.production.rps.ec.local`
 - `waf.acceptance.rps-4.ec.local`

On each RPS Virtual Machine, a *Proxy* container **MUST** exist for each
environment.

Those containers **MUST** have their host name set to `proxy.<env>.<vm fqdn>`, for
example:

 - `proxy.production.rps.ec.local`
 - `proxy.acceptance.rps-4.ec.local`

They **MUST** have the following host directory bind-mounted:

| Host path | Container path |
| --- | --- |
| `/etc/nginx/certs` | `/etc/nginx/certs` |
| `/var/www/acme-challenge` | `/var/www/acme-challenge` |

The *Traefik®*, *WAF* and *Proxy* containers **MUST** be in the same Docker Network.

## CMDB

**In order to differentiate `external`/`internal` clusters:**

Netbox tags of the form `<bubble>_<view>` **MUST** exist, for example:

 - `mybubble_external`
 - `mybubble_internal`

The following Virtual Machines **MUST** be tagged with:

| Virtual Machine | Tags |
| --- | --- |
| `rps.ec.local` | `mybubble_external` |
| `rps-2.ec.local` | `mybubble_external` |
| `rps-3.ec.local` | `mybubble_internal` |
| `rps-4.ec.local` | `mybubble_internal` |

**In order to correctly serve RPS mappings:**

Netbox tags of the form `<bubble>_<gateway>_<env>` **MUST** exist, for example:

- `mybubble_webcloud_production`
- `mybubble_intracloud_acceptance`

The environment `nms` and gateway `tech` **MUST** exist, for example:

- `mybubble_tech_nms`

The following Virtual Machines **MUST** be tagged with:

| Virtual Machine | Tags |
| --- | --- |
| `rps.ec.local` | `mybubble_webcloud_*` |
| `rps-2.ec.local` | `mybubble_webcloud_*` |
| `rps-3.ec.local` | `mybubble_intracloud_*` & `mybubble_tech_nms` |
| `rps-4.ec.local` | `mybubble_intracloud_*` & `mybubble_tech_nms` |

RPS Mappings to be served **MUST** be tagged with only one of those tags.

The following mappings **SHOULD** exist:

| Source URL | Target URL | Tag | Other |
| --- | --- | --- | --- |
| `https://netbox.<bubble>.nms.tech.ec.europa.eu/` | `http://netbox.ec.local:8080` | `<bubble>_tech_nms` | N/A |
| `https://awx.<bubble>.nms.tech.ec.europa.eu/` | `http://awx.ec.local:8013` | `<bubble>_tech_nms` | With extra protocol `Websocket` |
| `https://openobserve.<bubble>.nms.tech.ec.europa.eu/` | `http://openobserve.ec.local:5080` | `<bubble>_tech_nms` | N/A |
