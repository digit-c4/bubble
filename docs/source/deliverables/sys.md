# SYS Squad deliverables

The key words “MUST”, “MUST NOT”, “REQUIRED”, “SHALL”, “SHALL NOT”, “SHOULD”,
“SHOULD NOT”, “RECOMMENDED”, “MAY”, and “OPTIONAL” in those documents are to be
interpreted as described in [RFC 2119](https://www.ietf.org/rfc/rfc2119.txt).

## DNS

For each environment, the following DNS record **SHOULD** exist:

  - `netbox.ec.local`
  - `awx.ec.local`
  - `openobserve.ec.local`
  - `prometheus.ec.local`
  - `alertmanager.ec.local`
  - `proxy.ec.local`
  - `rps.ec.local`
  - `rps-2.ec.local`
  - `rps-3.ec.local`
  - `rps-4.ec.local`
  - `teleport.ec.local`
  - `dns.ec.local`
  - `dns-2.ec.local`

## CMDB

### Virtual Machines

The following virtual machines **SHOULD** exist:

  - `rps.ec.local`
  - `rps-2.ec.local`
  - `rps-3.ec.local`
  - `rps-4.ec.local`
  - `teleport.ec.local`
  - `netbox.ec.local`
  - `awx.ec.local`
  - `syslog-prometheus-alertmanager-openobserve.ec.local`
  - `dns.ec.local`
  - `dns-2.ec.local`

### Services

The following services **MUST** be reachable:

| Service | Endpoint |
| --- | --- |
| Netbox | `http://netbox.ec.local:8080` |
| Netbox Backup Agent | `http://backup.ec.local:1881` |
| AWX | `http://awx.ec.local:8013` |
| OpenObserve | `http://openobserve.ec.local:5080` |
| Prometheus | `http://prometheus.ec.local:9090`|
| AlertManager | `http://alertmanager.ec.local:9093` |
| Squid | `http://proxy.ec.local:8012` & `http://proxy.ec.local:3128` |

The following services **MUST** be configured inside netbox:
  - `nginx`
  - `teleport`
  - `syslog_server`
  - `openobserve`
  - `prometheus`
  - `alertmanager`
  - `bind`
  - `bind_external`
  - `DNS_TCP`
  - `DNS_UDP`

### IPAM

The subnet **MUST** be configured inside the IPAM section of Netbox.
