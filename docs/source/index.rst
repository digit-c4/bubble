Bubble Documentation
====================

Introduction
------------

A bubble is a piece of infrastructure that aims to provide secured access,
monitoring, and other management tools to the user's applications.

A bubble typically have a local network, and 2 gateways, namely:

* EXTERNAL: receive traffic from the internet
* INTERNAL: receive traffic from the EC intranet

.. image:: /RPS_LZ_Design_Proposal.png
   :alt: OVH Bubble architecture schema
   :align: center

Setup
-----

Setting up a new bubble is a cross-squad operation. Each squad provides one or
more service used to manage the bubble and the associated user applications:

* **SYS:**
   * Syslog
   * NTP
   * AWX
   * Prometheus / Grafana / AlertManager
* **DNS:**
   * BIND9
* **CMDB:**
   * Netbox
* **RPS:**
   * NGINX+
* Undefined:
   * Hashicorp Vault

Configuration
-------------

Each provided service should be delivered with a minimal configuration:

* Virtual Machines are referenced in Netbox
* Monitoring via Prometheus is active
* Logs are forwarded to the local Syslog
* AWX is provisioned with the necessary Ansible playbooks
* User applications are served via the Reverse Proxy

Within the bubble, the local network use the ``.ec.local`` DNS zone. It is to be
used for internal use cases (e.g. an Ansible Playbook querying the Netbox API).

The following FQDNs are mandatory:

* ``netbox.ec.local``
* ``awx.ec.local``
* ``syslog.ec.local``
* ``dns.ec.local``
* ``proxy.ec.local``

Bootstrap
---------

Setting up a new bubble is done in multiple phases, each phase done by a
specific squad.

.. diagrams:: /_diagrams/bubble_bootstrap.py

.. diagrams:: /_diagrams/bubble_galaxy.py

The following configuration should be implemented the same way on each bubble:

* a fixed FQDN for Netbox, AWX, Syslog, ...
* using a ``.ec.local`` DNS zone
* eventually using an extra ``.tech.ec.europa.eu`` DNS zone, to be added via the ``resolv.conf`` file

Architecture
------------

.. raw:: html
   :file: _diagrams/architecture.drawio.html

Specification
-------------

.. toctree::
   :maxdepth: 2

   requirements/index
   deliverables/index

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

See also
========

* `AWX job provisionning <https://digit-c4.pages.code.europa.eu/awx-data/>`_
