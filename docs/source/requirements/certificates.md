# Certificates Squad requirements

The key words “MUST”, “MUST NOT”, “REQUIRED”, “SHALL”, “SHALL NOT”, “SHOULD”,
“SHOULD NOT”, “RECOMMENDED”, “MAY”, and “OPTIONAL” in those documents are to be
interpreted as described in [RFC 2119](https://www.ietf.org/rfc/rfc2119.txt).

## DNS

A DNS server **MUST** exist in the bubble.

The DNS zone `.ec.local` **MUST** exist to resolve virtual machines within the
bubble.

For each domain (Common Name and Subject Alternate Name if they exist) of a certificate that must be generated in this bubble, a DNS zone that matches the domain (CN or SAN) or the immediate parent domain (for example a zone example.com for m.example.com) **MUST** exist and the resolution of this zone **MUST** be possible through the ACME PKI chosen for the certificate.

### VIP

An IP address with the role `VIP` and the tag `vip_internal` **SHOULD** exist,
if we want to use internal CA services such as Commissign®.

### Services

The following services **MUST** be reachable:

| Service | Endpoint |
| --- | --- |
| Netbox | `http://netbox.ec.local:8080` |
| AWX | `http://awx.ec.local:8013` |

Each of the Certificate Authority/PKI services, defined in defaults or in the CA plugin **MUST** be reachable from the bubble.

## CMDB

The following Netbox plugins **MUST** be installed:

 - Netbox DNS Plugin
 - Netbox Certificates Plugin

## Certificates

The following Certificate Authority **SHOULD** be available for ACME challenge:

| CA | Condition |
| --- | --- |
| Letsencrypt® | if the bubble is reachable via `external` |
| GlobalSign® | if the bubble is reachable via `external` |
| Commissign® | if the bubble is reachable via `internal` |

The ACME directory for each of the CA **MUST** be reachable from AWX if the corresponding CA is available.

Automation **MUST** be present to create/renew certificates specified in Netbox.

## RPS
RPS hosts **SHOULD** be defined in the AWX inventory so that certificates can be deployed on them.

## WAF
WAF hosts **SHOULD** be defined in the AWX inventory so that certificates can be deployed on them.

## Hashicorp® Vault

An instance of Hashicorp® Vault **SHOULD** be reachable from AWX so that the secrets of the CA and certificates can be read/written. If not provided, default secrets will be used and new data won't be saved.
