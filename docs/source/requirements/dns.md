# DNS Squad requirements

The key words “MUST”, “MUST NOT”, “REQUIRED”, “SHALL”, “SHALL NOT”, “SHOULD”,
“SHOULD NOT”, “RECOMMENDED”, “MAY”, and “OPTIONAL” in those documents are to be
interpreted as described in [RFC 2119](https://www.ietf.org/rfc/rfc2119.txt).

## SYS

The following Virtual Machines  **MUST** exist:

  - `netbox.ec.local`
  - `awx.ec.local`
  - `dns-public.ec.local`
  - `dns-private.ec.local`


They **MAY** point to the same host.
They **MAY** be created by the SYS team.

The provided user (usually **snet**) **MUST** have rights to access the Docker daemon.

The provided user (usually **snet**) **MUST** have rights to access (read/write) to /opt/SNet/ in order to store DNS bind configuration files. Failure to do so, user **MAY** pick another directory in the user $HOME directory to proceed.

## Proxy

A Squid proxy **MUST** be available on the bubble.

The DNS record `proxy.ec.local` **MUST** resolve to the Squid proxy.

## LB

Two Load Balancers **MUST** be available on the bubble.

  - `External Load Balancer`
  - `Internal Load Balancer`


## SEC

The bubble **MUST** be reachable via `external` and `internal`.
The following Rules **MUST** be applied for external:

  - `ANY -> 53 (TCP_UDP)`

The following Rules **MUST** be applied for internal:

  - `ANY -> 53 (TCP_UDP)`


## CMDB

The following Netbox plugins **MUST** be installed:

 - Netbox DNS Plugin
 - Netbox RPS Plugin
 - Netbox Certificates Plugin
 - Netbox Docker Plugin

The Virtual IP Addresses of the External and Internal Load Balancers **MUST** exist:

  - vip_internal
  - vip_external

The Cluster type `AWS` **MUST** exist.

The Cluster name **MUST** be set as the Bubble Name in order to create and populate the DNS Nameservers/Zones/Records.