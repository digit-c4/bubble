# RPS Squad requirements

The key words “MUST”, “MUST NOT”, “REQUIRED”, “SHALL”, “SHALL NOT”, “SHOULD”,
“SHOULD NOT”, “RECOMMENDED”, “MAY”, and “OPTIONAL” in those documents are to be
interpreted as described in [RFC 2119](https://www.ietf.org/rfc/rfc2119.txt).

## DNS

The DNS zone `.ec.local` **MUST** exist to resolve virtual machines within the
bubble.

The DNS zone `.<bubble>.<external-suffix>` **SHOULD** exist if the bubble
is reachable via `external`.

The DNS zone `.<bubble>.<internal-suffix>` **SHOULD** exist if the bubble
is reachable via `internal`.

**NB:** The suffix **CAN** be:

| Site | External Suffix | Internal Suffix |
| --- | --- | --- |
| AWS | `.webcloud.ec.europa.eu` | `.intracloud.ec.europa.eu` |
| DC | `.tech.ec.europa.eu` | `.tech.ec.europa.eu` |

The following sub DNS zones **MAY** exist according to the client's
requirements:

  - `.acceptance.<bubble>.<external-suffix>`
  - `.dev.<bubble>.<external-suffix>`
  - `.test.<bubble>.<external-suffix>`
  - `.<env>.<bubble>.<external-suffix>`
  - `.acceptance.<bubble>.<internal-suffix>`
  - `.dev.<bubble>.<internal-suffix>`
  - `.test.<bubble>.<internal-suffix>`
  - `.<env>.<bubble>.<internal-suffix>`

For the bubble management, the following sub DNS zones **MUST** exist:

 - `.<bubble>.nms.tech.ec.europa.eu`

The following DNS records **MUST** exist:

  - `netbox.ec.local`
  - `awx.ec.local`
  - `openobserve.ec.local`
  - `prometheus.ec.local`
  - `alertmanager.ec.local`
  - `prometheus.ec.local`
  - `proxy.ec.local`

They **MAY** point to the same host.

## SYS

### Virtual Machines

The following virtual machines **SHOULD** exist:

| FQDN | View | Condition |
| --- | --- | --- |
| `rps.ec.local` | EXTERNAL | if the bubble is reachable via `external` |
| `rps-2.ec.local` | EXTERNAL | if the bubble is reachable via `external` |
| `rps-3.ec.local` | INTERNAL | if the bubble is reachable via `internal` |
| `rps-4.ec.local` | INTERNAL | if the bubble is reachable via `internal` |

Their FQDN **MUST** remain the same. This means that if the bubble is only
reachable via `internal`, and only 2 VM are deployed, they **MUST** still be
named `rps-3.ec.local` and `rps-4.ec.local`.

They **MUST** have the role `RPS` assigned to them.

Every VM inside the bubble's network **MUST** be reachable via the `.ec.local`
DNS zone, on all TCP ports.

### VIP

An IP address with the role `VIP` and the tag `vip_external` **SHOULD** exist,
if the bubble is reachable via `external`.

An IP address with the role `VIP` and the tag `vip_internal` **SHOULD** exist,
if the bubble is reachable via `internal`.

### Services

The following services **MUST** be reachable:

| Service | Endpoint |
| --- | --- |
| Netbox | `http://netbox.ec.local:8080` |
| AWX | `http://awx.ec.local:8013` |
| OpenObserve | `http://openobserve.ec.local:5080` |
| Prometheus | `http://prometheus.ec.local:9090`|
| AlertManager | `http://alertmanager.ec.local:9093` |
| Squid | `http://proxy.ec.local:8012` & `http://proxy.ec.local:3128` |

### Docker

The [Netbox Docker Agent](https://github.com/saashup/netbox-docker-agent)
**MUST** be installed and in a **Running** state on every RPS virtual machine.

Containers output (stdout/stderr) **MUST** be forwarded to:

 - the bubble's OpenObserve
 - the European Commission's Splunk instance, via the bubble's syslog

## CMDB

The following Netbox plugins **MUST** be installed:

 - Netbox DNS Plugin
 - Netbox RPS Plugin
 - Netbox Certificates Plugin
 - Netbox Docker Plugin

## Certificates

The following Certificate Authority **SHOULD** be available for ACME challenge:

| CA | Condition |
| --- | --- |
| Letsencrypt® | if the bubble is reachable via `external` |
| GlobalSign® | if the bubble is reachable via `external` |
| Commissign® | if the bubble is reachable via `internal` |

Automation **MUST** be present to create/renew certificates specified in Netbox.
