# SYS Squad requirements

The key words “MUST”, “MUST NOT”, “REQUIRED”, “SHALL”, “SHALL NOT”, “SHOULD”,
“SHOULD NOT”, “RECOMMENDED”, “MAY”, and “OPTIONAL” in those documents are to be
interpreted as described in [RFC 2119](https://www.ietf.org/rfc/rfc2119.txt).

## DNS

The DNS zone `.ec.local` **MUST** exist to resolve virtual machines within the
bubble.

The DNS zone `.<bubble>.<external-suffix>` **SHOULD** exist if the bubble
is reachable via `external`.

The DNS zone `.<bubble>.<internal-suffix>` **SHOULD** exist if the bubble
is reachable via `internal`.

The following DNS records **MUST** exist:

  - `netbox.ec.local`
  - `awx.ec.local`
  - `openobserve.ec.local`
  - `prometheus.ec.local`
  - `alertmanager.ec.local`
  - `syslog.ec.local`
  - `dns.ec.local`
  - `prometheus.ec.local`
  - `proxy.ec.local`

They **MAY** point to the same host.
They **MAY** be created by the SYS team.

The following DNS records **SHOULD** exist in order to be able to resolve from extern:

  - `netbox.<bubble>.nms.tech.ec.europa.eu`
  - `awx.<bubble>.nms.tech.ec.europa.eu`
  - `openobserve.<bubble>.nms.tech.ec.europa.eu`
  - `prometheus.<bubble>.nms.tech.ec.europa.eu`
  - `alertmanager.<bubble>.nms.tech.ec.europa.eu`

The DNS resolver **MUST** exist in order to be able to resolve external records.

## Proxy

A Squid proxy **MUST** be available on the bubble.

The DNS record `proxy.ec.local` **MUST** resolve to the Squid proxy.

## LB

Two Load Balancers **MUST** be available on the bubble.

  - `External Load Balancer`
  - `Internal Load Balancer`


## SEC

The bubble **MUST** be reachable via `external` and `internal`.
The following Rules **MUST** be applied for external:

  - `ANY -> 443`
  - `ANY -> 80`
  - `ANY -> 8012`

The following Rules **MUST** be applied for internal:

  - `ANY -> 443`
  - `ANY -> 80`

The following Rules **MUST** be applied for bubble internal:
  - `ANY -> ALL`

## RPS

The following Mapping **MUST** be exist:

| Source URL | Target URL |
| --- | --- |
| `https://netbox.<bubble>.nms.tech.ec.europa.eu` | `http://netbox.ec.local:8080` |
| `https://awx.<bubble>.nms.tech.ec.europa.eu` | `http://awx.ec.local:8013` |
| `https://openobserve.<bubble>.nms.tech.ec.europa.eu` | `http://openobserve.ec.local:5080` |
| `https://prometheus.<bubble>.nms.tech.ec.europa.eu` | `http://prometheus.ec.local:9090` |

## CMDB

The following Netbox plugins **MUST** be installed:

 - Netbox DNS Plugin
 - Netbox RPS Plugin
 - Netbox Certificates Plugin
 - Netbox Docker Plugin

## Certificates

The following Certificate Authority **SHOULD** be available for ACME challenge:

| CA | Condition |
| --- | --- |
| Letsencrypt® | if the bubble is reachable via `external` |
| GlobalSign® | if the bubble is reachable via `external` |
| Commissign® | if the bubble is reachable via `internal` |

Automation **MUST** be present to create/renew certificates specified in Netbox.
