```mermaid
flowchart TD
 subgraph public["Public"]
        lbpub["rps1.ec.local
                    :443 :80
                    4C8GB"]
        rpspub["rps.ec.local
                    :443 :80
                    4C8GB"]
        dnspub["dns-public.ec.local
                    :53
                    2C8GB"]
        ras["ras
                    :22
                    2C8GB"]
  end
     subgraph service["Service"]
        netbox["netbox.ec.local
                    :8080
                    2C8GB"]
        awx["awx.ec.local
                    :8013
                    2C8GB"]
        proxy["proxy.ec.local
                    :8012
                    2C8GB"]
  end
 subgraph mgmt["Monitoring"]
        syslog["syslog.ec.local
                    :514
                    2C8GB"]
        prometheus["prometheus.ec.local
                    :9090
                    2C8GB"]
  end
 subgraph private["Private"]
        lbpriv["rps3.ec.local
                    :443 :80
                    4C8GB"]
        rpspriv["rps4.ec.local
                    :443 :80
                    4C8GB"]
        dnspriv["dns-private.ec.local
                    :53
                    2C8GB"]
  end
 subgraph Bubble["Bubble X - 10.66.X.0/27 - VMs"]
        private
        service
        mgmt
        public
      end
```
