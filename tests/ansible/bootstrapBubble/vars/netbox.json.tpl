{ "bubble": {
    "transit_gateway": {
        "id": "$AWS_TRANSIT_GATEWAY_ID",
        "name": "$AWS_TRANSIT_GATEWAY_NAME"
    },
    "provider_credentials": {
        "username": "$AWS_ACCESS_KEY",
        "password": "$AWS_SECRET_KEY"
    },
    "subnetRange": "$SUBNETRANGE",
    "provider": "aws",
    "name": "bootstrap-test",
    "region": "eu-west-1",
    "publicDnsDomain": "PUBLICDNSDOMAIN",
    "privateDnsDomain": "PRIVATEDNSDOMAIN",
    "usages": [
        "Test1"
    ],
    "routing": [
        {
            "dest": "10.0.0.0/8",
            "gateway_id": "$AWS_TRANSIT_GATEWAY_ID"
        }
    ],
    "securityRules": [
        {
            "proto": "tcp",
            "from_port": 80,
            "to_port": 80,
            "cidr_ip": "0.0.0.0/0"
        },
        {
            "proto": "tcp",
            "from_port": 443,
            "to_port": 443,
            "cidr_ip": "0.0.0.0/0"
        },
        {
            "proto": "tcp",
            "from_port": 22,
            "to_port": 22,
            "cidr_ip": "$SUBNETRANGE",
        },
        {
            "proto": "tcp",
            "from_port": 53,
            "to_port": 53,
            "cidr_ip": "0.0.0.0/0",
        },
        {
            "proto": "udp",
            "from_port": 53,
            "to_port": 53,
            "cidr_ip": "0.0.0.0/0",
        },
        {
            "proto": "tcp",
            "from_port": 8080,
            "to_port": 8080,
            "cidr_ip": "$SUBNETRANGE"
        },
        {
            "proto": "tcp",
            "from_port": 8013,
            "to_port": 8013,
            "cidr_ip": "$SUBNETRANGE"
        }
    ],
    "lbPublic": {
        "listeners": [
            {
                "Protocol": "TCP",
                "Port": 80,
                "DefaultActions": [{
                    "Type": "forward",
                    "TargetGroupName": "bootstrap-test-rps-public-80"
                }
                ],
            },
            {
                "Port": 443,
                "Protocol": "TCP",
                "DefaultActions": [{
                    "Type": "forward",
                    "TargetGroupName": "bootstrap-test-rps-public-443"
                }
                ]
            }
        ]
    },
    "lbPrivate": {
        "listeners": [
            {
                "Port": 80,
                "Protocol": "TCP",
                "DefaultActions": [{
                    "Type": "forward",
                    "TargetGroupName": "bootstrap-test-rps-private-80"
                }
                ]
            },
            {
                "Protocol": "TCP",
                "Port": 443,
                "DefaultActions": [{
                    "Type": "forward",
                    "TargetGroupName": "bootstrap-test-rps-private-443"
                }
                ]
            }
        ]
       
    },
    "bootstrapData": [
        {
            "serviceName": "dns-public",
            "hosts": [
                {
                    "name": "dns.ec.local",
                    "owner": "dns",
                    "ipAddress": "1.2.3.4",
                    "machineType": "t2.xlarge",
                    "network": "$AWS_SUBNET_ID",
                    "ssh_key": "$AWS_SSH_KEY",
                    "assignPublicIp": false
                },
                {
                    "name": "dns2.ec.local",
                    "owner": "dns",
                    "ipAddress": "1.2.3.5",
                    "machineType": "t2.xlarge",
                    "network": "$AWS_SUBNET_ID",
                    "ssh_key": "$AWS_SSH_KEY",
                    "assignPublicIp": false
                }

            ],
            "rpsMapping": [
                {
                    "source": "SOURCEURL",
                    "destination": "DESTINATIONURL"
                }
            ]
        },
        {
            "serviceName": "rps-public",
            "hosts": [
                {
                    "name": "rps1.ec.local",
                    "owner": "dns",
                    "ipAddress": "1.2.3.6",
                    "machineType": "t2.xlarge",
                    "network": "$AWS_SUBNET_ID",
                    "ssh_key": "$AWS_SSH_KEY",
                    "assignPublicIp": false
                },
                {
                    "name": "rps2.ec.local",
                    "owner": "dns",
                    "ipAddress": "1.2.3.7",
                    "machineType": "t2.xlarge",
                    "network": "$AWS_SUBNET_ID",
                    "ssh_key": "$AWS_SSH_KEY",
                    "assignPublicIp": false
                }

            ],
            "rpsMapping": [
                {
                    "source": "SOURCEURL",
                    "destination": "DESTINATIONURL"
                }
            ]
        },
        {
            "serviceName": "rps-private",
            "hosts": [
                {
                    "name": "rps3.ec.local",
                    "owner": "dns",
                    "ipAddress": "1.2.3.6",
                    "machineType": "t2.xlarge",
                    "network": "$AWS_SUBNET_ID",
                    "ssh_key": "$AWS_SSH_KEY",
                    "assignPublicIp": false
                },
                {
                    "name": "rps4.ec.local",
                    "owner": "dns",
                    "ipAddress": "1.2.3.7",
                    "machineType": "t2.xlarge",
                    "network": "$AWS_SUBNET_ID",
                    "ssh_key": "$AWS_SSH_KEY",
                    "assignPublicIp": false
                }
            ],
            "rpsMapping": [
                {
                    "source": "SOURCEURL",
                    "destination": "DESTINATIONURL"
                }
            ]
        }
    ]
}}
